﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Eletronuclear.DTDA.COVID_19.ENTIDADE;
using Eletronuclear.DTDA.COVID_19.DAL.Persistencia;

namespace Eletronuclear.DTDA.COVID_19.BLL.Repositorio
{
    public class repositorioUSUARIO
    {
        public entidadeUSUARIO Pesquisar(entidadeUSUARIO objUSUARIO)
        {
            dalUSUARIO dalUSUARIO = null;

            DataTable dt = null;

            try
            {
                dalUSUARIO = new dalUSUARIO();

                dt = dalUSUARIO.Pesquisar(objUSUARIO);

                if (dt.Rows.Count == 0)
                {
                    objUSUARIO.LOGIN = null;
                }
                else
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (!Convert.IsDBNull(dr[0]))
                            objUSUARIO.MATRICULA = dr[0].ToString();

                        if (!Convert.IsDBNull(dr[1]))
                            objUSUARIO.NOME_COMPLETO = dr[1].ToString();

                        if (!Convert.IsDBNull(dr[2]))
                            objUSUARIO.UO_SIGLA = dr[2].ToString();

                        if (!Convert.IsDBNull(dr[3]))
                            objUSUARIO.LOGIN = dr[3].ToString();

                        if (!Convert.IsDBNull(dr[4]))
                            objUSUARIO.CPF = dr[4].ToString();

                        if (!Convert.IsDBNull(dr[5]))
                            objUSUARIO.TIPO = dr[5].ToString();
                    }
                }                

                return objUSUARIO;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
