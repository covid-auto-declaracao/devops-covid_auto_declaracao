﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eletronuclear.DTDA.COVID_19.ENTIDADE
{
    public class entidadeTBAUTO_DECLARACAO
    {
        public int id { get; set; }

        public string login { get; set; }

        public DateTime dthlocal { get; set; }

        public string caminhopdf { get; set; }

    }
}
