﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eletronuclear.DTDA.COVID_19.ENTIDADE
{
    public class entidadeUSUARIO
    {
        public string MATRICULA { get; set; }

        public string UO_SIGLA { get; set; }

        public string NOME_COMPLETO { get; set; }

        public string CPF { get; set; }

        public string LOGIN { get; set; }

        public string TIPO { get; set; }
    }
}
