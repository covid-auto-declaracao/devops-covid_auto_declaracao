﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MpPublica.Master" AutoEventWireup="true" CodeBehind="frmCovid.aspx.cs" Inherits="Eletronuclear.DTDA.COVID_19.UI.Formularios.frmCovid" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript">

        function marcarCondicao1() {
            if ($('#ContentPlaceHolder1_Condicao1').prop("checked", true)) {
                $('#ContentPlaceHolder1_Condicao1').prop("checked", true);
                $('#ContentPlaceHolder1_Condicao2').prop("checked", false);
            }
        }

        function marcarCondicao2() {
            if ($('#ContentPlaceHolder1_Condicao2').prop("checked", true)) {
                $('#ContentPlaceHolder1_Condicao1').prop("checked", false);
                $('#ContentPlaceHolder1_Condicao2').prop("checked", true);
            }
        }


        //function marcarRio() {
        //    if ($('#ContentPlaceHolder1_Rio').prop("checked", true)) {
        //        $('#ContentPlaceHolder1_Rio').prop("checked", true);
        //        $('#ContentPlaceHolder1_Angra').prop("checked", false);
        //    }
        //}

        //function marcarAngra() {
        //    if ($('#ContentPlaceHolder1_Angra').prop("checked", true)) {
        //        $('#ContentPlaceHolder1_Rio').prop("checked", false);
        //        $('#ContentPlaceHolder1_Angra').prop("checked", true);
        //    }
        //}

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="messagealert" id="alert_container">
    </div>
    <form runat="server">
        <asp:ScriptManager ID="Scriptmanager1" runat="server" EnableScriptGlobalization="true">
        </asp:ScriptManager>
        <div class="textCentralizar">
            <p class="big paragrafo">
                Ao chefe da DSM.A
                <br />
            </p>
            <p class="big">
                Eu,&nbsp
                <asp:Label ID="Label3" runat="server" Text="*" ForeColor="Red" />
                <asp:TextBox ID="txtNome" runat="server" />
                , matrícula &nbsp
                <asp:Label ID="Label5" runat="server" Text="*" ForeColor="Red" />
                <asp:TextBox ID="txtMatricula" runat="server" />
                ,
                <br />
                inscrito no CPF: &nbsp
                <asp:Label ID="Label2" runat="server" Text="*" ForeColor="Red" />
                <asp:TextBox ID="txtCPF" runat="server" />
                , lotado na &nbsp
                <asp:Label ID="Label4" runat="server" Text="*" ForeColor="Red" />
                <asp:TextBox ID="txtUO" runat="server" />
                , declaro ter assinado a Declaração – Trabalho Remoto (COVID-19), constante da CGE 017/20,  em função de estar, naquele momento, enquadrado na seguinte condição.
            </p>
        </div>
        <div id="Tabela">
            <table class="table">
                <thead>
                    <tr>
                        <th>Condição </th>
                        <th>Portador </th>
                        <th>Coabitante </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <label for="lblGravidez">Gravidez</label>
                        </td>
                        <td>
                            <label class="checkbox-inline">
                                <input id="lblGravidezPort" type="checkbox" value="1" runat="server" />
                            </label>
                        </td>
                        <td>
                            <label class="checkbox-inline">
                                <input id="lblGravidezCoab" type="checkbox" value="1" runat="server" />
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lblLactante">Lactante</label>
                        </td>
                        <td>
                            <label class="checkbox-inline">
                                <input id="lblLactantePort" type="checkbox" value="1" runat="server" />
                            </label>
                        </td>
                        <td>
                            <label class="checkbox-inline">
                                <input id="lblLactanteCoab" type="checkbox" value="1" runat="server" />
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lblTransp">Transplantados</label>
                        </td>
                        <td>
                            <label class="checkbox-inline">
                                <input id="lblTranspPort" type="checkbox" value="1" runat="server" />
                            </label>
                        </td>
                        <td>
                            <label class="checkbox-inline">
                                <input id="lblTranspCoab" type="checkbox" value="1" runat="server" />
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lblQuimio">Pessoas que estejam em uso de medicamentos imunodepressores ou quimioterápicos</label>
                        </td>
                        <td>
                            <label class="checkbox-inline">
                                <input id="lblQuimioPort" type="checkbox" value="1" runat="server" />
                            </label>
                        </td>
                        <td>
                            <label class="checkbox-inline">
                                <input id="lblQuimioCoab" type="checkbox" value="1" runat="server" />
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lblDiabetes">Diabetes em maiores de 60 anos</label>
                        </td>
                        <td>
                            <label class="checkbox-inline">
                                <input id="lblDiabetesPort" type="checkbox" value="1" runat="server" />
                            </label>
                        </td>
                        <td>
                            <label class="checkbox-inline">
                                <input id="lblDiabetesCoab" type="checkbox" value="1" runat="server" />
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lblHiper">Hipertensão em maiores de 60 anos</label>
                        </td>
                        <td>
                            <label class="checkbox-inline">
                                <input id="lblHiperPort" type="checkbox" value="1" runat="server" />
                            </label>
                        </td>
                        <td>
                            <label class="checkbox-inline">
                                <input id="lblHiperCoab" type="checkbox" value="1" runat="server" />
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lblTaba">Tabagismo em maiores de 60 anos</label>
                        </td>
                        <td>
                            <label class="checkbox-inline">
                                <input id="lblTabaPort" type="checkbox" value="1" runat="server" />
                            </label>
                        </td>
                        <td>
                            <label class="checkbox-inline">
                                <input id="lblTabaCoab" type="checkbox" value="1" runat="server" />
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lblCardio">Portador de doenças cardíacas</label>
                        </td>
                        <td>
                            <label class="checkbox-inline">
                                <input id="lblCardioPort" type="checkbox" value="1" runat="server" />
                            </label>
                        </td>
                        <td>
                            <label class="checkbox-inline">
                                <input id="lblCardioCoab" type="checkbox" value="1" runat="server" />
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lblRenais">Portador de doenças renais crônicas</label>
                        </td>
                        <td>
                            <label class="checkbox-inline">
                                <input id="lblRenaisPort" type="checkbox" value="1" runat="server" />
                            </label>
                        </td>
                        <td>
                            <label class="checkbox-inline">
                                <input id="lblRenaisCoab" type="checkbox" value="1" runat="server" />
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lblAuto">Portador de doenças autoimunes</label>
                        </td>
                        <td>
                            <label class="checkbox-inline">
                                <input id="lblAutoPort" type="checkbox" value="1" runat="server" />
                            </label>
                        </td>
                        <td>
                            <label class="checkbox-inline">
                                <input id="lblAutoCoab" type="checkbox" value="1" runat="server" />
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lblHema">Portador de doenças hematológicas</label>
                        </td>
                        <td>
                            <label class="checkbox-inline">
                                <input id="lblHemaPort" type="checkbox" value="1" runat="server" />
                            </label>
                        </td>
                        <td>
                            <label class="checkbox-inline">
                                <input id="lblHemaCoab" type="checkbox" value="1" runat="server" />
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lblAsma">Portador de asma brônquica moderada a grave</label>
                        </td>
                        <td>
                            <label class="checkbox-inline">
                                <input id="lblAsmaPort" type="checkbox" value="1" runat="server" />
                            </label>
                        </td>
                        <td>
                            <label class="checkbox-inline">
                                <input id="lblAsmaCoab" type="checkbox" value="1" runat="server" />
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lblBronquite">Portador de bronquite crônica</label>
                        </td>
                        <td>
                            <label class="checkbox-inline">
                                <input id="lblBronquitePort" type="checkbox" value="1" runat="server" />
                            </label>
                        </td>
                        <td>
                            <label class="checkbox-inline">
                                <input id="lblBronquiteCoab" type="checkbox" value="1" runat="server" />
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lblEnfisema">Portador de enfisema pulmonar</label>
                        </td>
                        <td>
                            <label class="checkbox-inline">
                                <input id="lblEnfisemaPort" type="checkbox" value="1" runat="server" />
                            </label>
                        </td>
                        <td>
                            <label class="checkbox-inline">
                                <input id="lblEnfisemaCoab" type="checkbox" value="1" runat="server" />
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lblCrianca">Responsáveis por criança(s) (de até 12 anos incompletos) que não tenham condição de ficar em outro ambiente de segurança aos cuidados de terceiros.</label>
                        </td>
                        <td colspan="2">
                            <label class="checkbox-inline">
                                <input id="lblCriancaPort" type="checkbox" value="1" runat="server" />
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lblIdoso">Responsáveis por idosos que não tenham condição de ficar em outro ambiente de segurança aos cuidados de terceiros.</label>
                        </td>
                        <td colspan="2">
                            <label class="checkbox-inline">
                                <input id="lblIdosoPort" type="checkbox" value="1" runat="server" />
                            </label>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="textCentralizar">
                <p class="big paragrafo">
                    Declaro que, atualmente: &nbsp
                    <input id="Condicao1" type="checkbox" value="1" runat="server" checked="checked" onclick='marcarCondicao1();' />
                    &nbsp esta condição se mantém &nbsp
                    <input id="Condicao2" type="checkbox" value="2" runat="server" onclick='marcarCondicao2();' />
                    &nbsp não me enquadro mais nas condições acima.
                </p>
            </div>
            <div class="textCentralizar">
                <p class="big paragrafo">
                    <input id="Ciente" type="checkbox" value="1" runat="server" />
                    <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red" />
                    &nbsp
                    Estou ciente que minha declaração será avaliada pela DSM.A, junto com meu histórico médico-social, e usada para subsidiar as decisões da alta direção concernentes ao retorno das atividades presenciais.  Também estou ciente que poderei ser solicitado a apresentar laudo médico comprobatório. A prestação de declaração falsa configura falta funcional, estando sujeita às sanções previstas no Código de Conduta e Ética do Grupo Eletrobras.
                </p>
            </div>

            <div class="textCentralizar">
                <p class="big paragrafo">
                    Local:
                    <asp:Label ID="Label6" runat="server" Text="*" ForeColor="Red" />
                    &nbsp
                    <input id="Rio" type="checkbox" value="1" runat="server" />
                    &nbsp Rio de Janeiro  &nbsp
                    <input id="Angra" type="checkbox" value="2" runat="server" />
                    &nbsp Angra dos Reis 
                </p>

                <p class="big paragrafo">
                    Data:
                    &nbsp
                     <%:DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")%>
                </p>
            </div>

            <div class="textLocalData">
                <p class="big paragrafo">
                    Assinado Eletronicamente através do Login:
                    <asp:Label ID="lblLogin" runat="server" />
                </p>
                <p class="big paragrafo">
                    Anexo da CGE nº 029/2020
                </p>
            </div>

            <div class="btnCentralizar">
                <asp:Button CssClass="btn btn-success" ID="btnSalvar" runat="server" Text="Gerar Auto Declaração" OnClick="btnSalvar_Click" />
            </div>
        </div>


    </form>




</asp:Content>
