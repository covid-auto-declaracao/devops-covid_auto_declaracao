﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Eletronuclear.DTDA.COVID_19.UI
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~//Formularios//frmCovid.aspx");
            }
            catch (Exception ex)
            {
                throw;
            }

        }
    }
}