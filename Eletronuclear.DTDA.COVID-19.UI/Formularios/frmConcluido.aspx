﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MpPublica.Master" AutoEventWireup="true" CodeBehind="frmConcluido.aspx.cs" Inherits="Eletronuclear.DTDA.COVID_19.UI.Formularios.frmConcluido" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form runat="server">
        <div>
            <div class="pgConcluidoDiv pgConcluidoTexto centralizarDiv">
                <asp:Label ID="Label1" runat="server" Text="Seus dados foram enviados para área médica com sucesso, Agradecemos sua participação!"></asp:Label>
                <br />
            </div>

            <div class="pgConcluidoDiv centralizarDiv">
                <asp:Button CssClass="btn btn-primary" ID="btnSair" runat="server" Text="Sair" OnClick="btnSair_Click" />

            </div>
        </div>


    </form>
</asp:Content>
