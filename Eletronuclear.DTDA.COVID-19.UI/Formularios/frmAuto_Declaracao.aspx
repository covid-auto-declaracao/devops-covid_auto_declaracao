﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmAuto_Declaracao.aspx.cs" Inherits="Eletronuclear.DTDA.COVID_19.UI.Formularios.frmAuto_Declaracao" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Content/css/site.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">

        <div class="textCentralizar">
            
            <h3 style="text-align: center;">
                <strong>Formulário Auto Declaração </strong>
            </h3>
            <br />
            <br />
            <h5>Ao chefe da DSM.A
                    <br />
                <br />
                Eu,
                    <asp:Label ID="lblnome" runat="server" />
                , matrícula 
                    <asp:Label ID="lblmatricula" runat="server" />
                ,
                    inscrito no CPF: 
                   <asp:Label ID="lblcpf" runat="server" />
                , lotado na 
                    <asp:Label ID="lbluo" runat="server" />
                , declaro ter assinado a Declaração – Trabalho Remoto (COVID-19), constante da CGE 017/20,  em função de estar, naquele momento, enquadrado na seguinte condição.
                <br />
                <br />
            </h5>

        </div>

        <div id="Tabela">
            <h5>
                <table border="1">
                    <thead>
                        <tr>
                            <th colspan="6" style="padding-left: 7px;">Condição </th>
                            <th style="text-align: center;">Portador </th>
                            <th style="text-align: center;">Coabitante </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="6" style="padding-left: 7px;">
                                <label for="lblGravidez"> a) Gravidez</label>
                            </td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblGravidezPort" runat="server" />
                            </td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblGravidezCoab" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" style="padding-left: 7px;">
                                <label for="lblLactante"> b) Lactante (até 02 anos de idade)</label>
                            </td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblLactantePort" runat="server" />
                            </td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblLactanteCoab" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" style="padding-left: 7px;">
                                <label for="lblTransp"> c) Transplantados</label>
                            </td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblTranspPort" runat="server" />
                            </td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblTranspCoab" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" style="padding-left: 7px;">
                                <label for="lblQuimio"> d) Pessoas que estejam em uso de medicamentos imunodepressores ou quimioterápicos</label>
                            </td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblQuimioPort" runat="server" />
                            </td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblQuimioCoab" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" style="padding-left: 7px;">
                                <label for="lblDiabetes"> e) Diabetes</label>
                            </td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblDiabetesPort" runat="server" />
                            </td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblDiabetesCoab" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" style="padding-left: 7px;">
                                <label for="lblHiper"> f) Hipertensão</label>
                            </td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblHiperPort" runat="server" />
                            </td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblHiperCoab" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" style="padding-left: 7px;">
                                <label for="lblTaba"> g) Tabagismo</label>
                            </td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblTabaPort" runat="server" />
                            </td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblTabaCoab" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" style="padding-left: 7px;">
                                <label for="lblCardio"> h) Portador de doenças cardíacas</label>
                            </td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblCardioPort" runat="server" />
                            </td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblCardioCoab" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" style="padding-left: 7px;">
                                <label for="lblRenais"> i) Portador de doenças renais crônicas</label>
                            </td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblRenaisPort" runat="server" />
                            </td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblRenaisCoab" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" style="padding-left: 7px;">
                                <label for="lblAuto"> j) Portador de doenças autoimunes</label>
                            </td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblAutoPort" runat="server" />
                            </td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblAutoCoab" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" style="padding-left: 7px;">
                                <label for="lblHema"> k) Portador de doenças hematológicas</label>
                            </td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblHemaPort" runat="server" />
                            </td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblHemaCoab" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" style="padding-left: 7px;">
                                <label for="lblAsma"> l) Portador de asma brônquica moderada a grave</label>
                            </td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblAsmaPort" runat="server" />
                            </td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblAsmaCoab" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" style="padding-left: 7px;">
                                <label for="lblBronquite"> m) Portador de bronquite crônica</label>
                            </td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblBronquitePort" runat="server" />
                            </td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblBronquiteCoab" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" style="padding-left: 7px;">
                                <label for="lblEnfisema"> n) Portador de enfisema pulmonar</label>
                            </td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblEnfisemaPort" runat="server" />
                            </td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblEnfisemaCoab" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" style="padding-left: 7px;">
                                <label for="lblCrianca"> o) Responsáveis por criança(s) (de até 12 anos incompletos) que não tenham condição de ficar em outro ambiente de segurança aos cuidados de terceiros</label>
                            </td>
                            <td colspan="2" style="text-align: center;">
                                <asp:Label ID="lblCriancaPort" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" style="padding-left: 7px;">
                                <label for="lblIdoso"> p) Responsáveis por idosos que não tenham condição de ficar em outro ambiente de segurança aos cuidados de terceiros</label>
                            </td>
                            <td colspan="2" style="text-align: center;">
                                <asp:Label ID="lblIdosoPort" runat="server" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </h5>

            <div class="textCentralizar">
                <h5>
                    <br />
                    Declaro que, atualmente: 
                   <asp:Label ID="lblcondicao" runat="server" />
            </div>
            <div class="textCentralizar">
                <h5>
                    <br />
                    Estou ciente que minha declaração será avaliada pela DSM.A, junto com meu histórico médico-social, e usada para subsidiar as decisões da alta direção concernentes ao retorno das atividades presenciais.  Também estou ciente que poderei ser solicitado a apresentar laudo médico comprobatório. A prestação de declaração falsa configura falta funcional, estando sujeita às sanções previstas no Código de Conduta e Ética do Grupo Eletrobras.
                
                </h5>
            </div>

            <div class="textLocalData">
                <br />
                <%--<h5 style="text-align: center;">Local, Data--%>
                <%--</h5>--%>

                <h5 style="text-align: center;">
                    <asp:Label ID="lblcidade" runat="server" />
                    ,
                     <asp:Label Style="text-align: center;" ID="lbldata" runat="server" />
                </h5>
                
                <br />
                <h5 style="text-align: center;">Assinado Eletronicamente através do Login:
                    <asp:Label ID="lblLogin" runat="server" />
                </h5>

                <h5 style="text-align: center;">Anexo da CGE nº 029/2020
                </h5>

            </div>

        </div>
    </form>
</body>
</html>
