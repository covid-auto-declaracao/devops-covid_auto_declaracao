﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Eletronuclear.DTDA.COVID_19.BLL.RegraNegocio;
using Eletronuclear.DTDA.COVID_19.ENTIDADE;
using System.Security.Principal;

namespace Eletronuclear.DTDA.COVID_19.UI.Formularios
{
    public partial class frmCovid : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string logUsu = UsuarioLogin();
                // logUsu = "gladsil";
                BuscarUsuario(logUsu);
            }
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                regranegocioMetodosMensagensSistemas regranegocioMetodosMensagensSistemas = new regranegocioMetodosMensagensSistemas();

                if (string.IsNullOrEmpty(txtNome.Text.Trim()) || string.IsNullOrEmpty(txtMatricula.Text.Trim()) || string.IsNullOrEmpty(txtCPF.Text.Trim()) || string.IsNullOrEmpty(txtUO.Text.Trim()))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), regranegocioMetodosMensagensSistemas.ShowMessage("Favor preencher os campos com seus dados pessoais.", regranegocioMetodosMensagensSistemas.MessageType.Warning), true);
                }
                else if (Ciente.Checked == false)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), regranegocioMetodosMensagensSistemas.ShowMessage("Favor marcar Estou ciente para gerar o arquivo.", regranegocioMetodosMensagensSistemas.MessageType.Warning), true);
                }
                else if (Rio.Checked == false && Angra.Checked == false)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), regranegocioMetodosMensagensSistemas.ShowMessage("Favor marcar sua localidade.", regranegocioMetodosMensagensSistemas.MessageType.Warning), true);
                }
                else
                {
                    this.Salvar();
                }

            }
            catch (Exception)
            {

                throw;
            }
        }

        #region Metodos

        public string UsuarioLogin()
        {
            HttpContext ctx = System.Web.HttpContext.Current;

            string userName = ctx.Request.ServerVariables["LOGON_USER"].ToString();
            string logUsu = (userName).Remove(0, userName.IndexOf("\\") + 1).ToUpper();

            return logUsu;
        }

        private void BuscarUsuario(string login)
        {
            entidadeUSUARIO objUSUARIO;
            regranegocioFRMCOVID regranegocioFRMCOVID;

            try
            {
                objUSUARIO = new entidadeUSUARIO();
                regranegocioFRMCOVID = new regranegocioFRMCOVID();

                objUSUARIO.LOGIN = login;

                objUSUARIO = regranegocioFRMCOVID.Pesquisar(objUSUARIO);

                if (objUSUARIO.LOGIN == null)
                {
                    Response.Redirect("~//Formularios//frmAcessoNegado.aspx");
                }
                else
                {
                    //if (objUSUARIO.TIPO == "TERCEIRIZADO")
                    //{ Response.Redirect("~//Formularios//frmAcessoNegado.aspx"); }
                    //else
                    //{
                    txtNome.Text = objUSUARIO.NOME_COMPLETO.ToUpper();
                    txtCPF.Text = objUSUARIO.CPF;
                    txtMatricula.Text = objUSUARIO.MATRICULA.ToUpper();
                    txtUO.Text = objUSUARIO.UO_SIGLA.ToUpper();
                    lblLogin.Text = login;
                    //}
                }

            }
            catch (Exception)
            {

                throw;
            }
        }

        private void Salvar()
        {
            try
            {
                regranegocioMetodosMensagensSistemas regranegocioMetodosMensagensSistemas = new regranegocioMetodosMensagensSistemas();

                //Dados pessoais
                string nome = txtNome.Text;
                string matricula = txtMatricula.Text;
                string cpf = txtCPF.Text;
                string uo = txtUO.Text;

                //Grid
                string gravidezPort = lblGravidezPort.Checked ? "X" : "-";
                string gravidezCoab = lblGravidezCoab.Checked ? "X" : "-";

                string lactantePort = lblLactantePort.Checked ? "X" : "-";
                string lactanteCoab = lblLactanteCoab.Checked ? "X" : "-";

                string transpPort = lblTranspPort.Checked ? "X" : "-";
                string transpCoab = lblTranspCoab.Checked ? "X" : "-";

                string quimioPort = lblQuimioPort.Checked ? "X" : "-";
                string quimioCoab = lblQuimioCoab.Checked ? "X" : "-";

                string diabetesPort = lblDiabetesPort.Checked ? "X" : "-";
                string diabetesCoab = lblDiabetesCoab.Checked ? "X" : "-";

                string hiperPort = lblHiperPort.Checked ? "X" : "-";
                string hiperCoab = lblHiperCoab.Checked ? "X" : "-";

                string tabaPort = lblTabaPort.Checked ? "X" : "-";
                string tabaCoab = lblTabaCoab.Checked ? "X" : "-";

                string cardioPort = lblCardioPort.Checked ? "X" : "-";
                string cardioCoab = lblCardioCoab.Checked ? "X" : "-";

                string renaisPort = lblRenaisPort.Checked ? "X" : "-";
                string renaisCoab = lblRenaisCoab.Checked ? "X" : "-";

                string autoPort = lblAutoPort.Checked ? "X" : "-";
                string autoCoab = lblAutoCoab.Checked ? "X" : "-";

                string hemaPort = lblHemaPort.Checked ? "X" : "-";
                string hemaCoab = lblHemaCoab.Checked ? "X" : "-";

                string asmaPort = lblAsmaPort.Checked ? "X" : "-";
                string asmaCoab = lblAsmaCoab.Checked ? "X" : "-";

                string bronquitePort = lblBronquitePort.Checked ? "X" : "-";
                string bronquiteCoab = lblBronquiteCoab.Checked ? "X" : "-";

                string enfisemaPort = lblEnfisemaPort.Checked ? "X" : "-";
                string enfisemaCoab = lblEnfisemaCoab.Checked ? "X" : "-";

                string criancaPort = lblCriancaPort.Checked ? "X" : "-";

                string idosoPort = lblIdosoPort.Checked ? "X" : "-";

                //Condicao
                string condicao1 = Condicao1.Checked.ToString();
                string condicao2 = Condicao2.Checked.ToString();
                string ciente = Ciente.Checked ? "X" : "-";

                //Cidade
                string rio = Rio.Checked.ToString();
                string angra = Angra.Checked.ToString();

                string login = lblLogin.Text;

                ClientScript.RegisterStartupScript(typeof(Page), "", "window.open('frmAuto_Declaracao.aspx?nome=" + nome + "&matricula=" + matricula + "&cpf=" + cpf + "&uo=" + uo + "&gravidezPort=" + gravidezPort + "&gravidezCoab=" + gravidezCoab + "&lactantePort=" + lactantePort + "&lactanteCoab=" + lactanteCoab + "&transpPort=" + transpPort + "&transpCoab=" + transpCoab + "&quimioPort=" + quimioPort + "&quimioCoab=" + quimioCoab + "&diabetesPort=" + diabetesPort + "&diabetesCoab=" + diabetesCoab + "&hiperPort=" + hiperPort + "&hiperCoab=" + hiperCoab + "&tabaPort=" + tabaPort + "&tabaCoab=" + tabaCoab + "&cardioPort=" + cardioPort + "&cardioCoab=" + cardioCoab + "&renaisPort=" + renaisPort + "&renaisCoab=" + renaisCoab + "&autoPort=" + autoPort + "&autoCoab=" + autoCoab + "&hemaPort=" + hemaPort + "&hemaCoab=" + hemaCoab + "&asmaPort=" + asmaPort + "&asmaCoab=" + asmaCoab + "&bronquitePort=" + bronquitePort + "&bronquiteCoab=" + bronquiteCoab + "&enfisemaPort=" + enfisemaPort + "&enfisemaCoab=" + enfisemaCoab + "&criancaPort=" + criancaPort + "&idosoPort=" + idosoPort + "&condicao1=" + condicao1 + "&condicao2=" + condicao2 + "&ciente=" + ciente + "&rio=" + rio + "&angra=" + angra + "&login=" + login + "');", true);

                ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), regranegocioMetodosMensagensSistemas.ShowMessage("Seus dados foram enviados para área médica com sucesso, Agradecemos sua participação!", regranegocioMetodosMensagensSistemas.MessageType.Success), true);

                btnSalvar.Enabled = false;

            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }
}