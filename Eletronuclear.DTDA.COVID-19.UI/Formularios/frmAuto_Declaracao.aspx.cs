﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using Eletronuclear.DTDA.COVID_19.BLL.RegraNegocio;
using Eletronuclear.DTDA.COVID_19.ENTIDADE;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using iTextSharp.tool.xml.pipeline.html;
using iTextSharp.tool.xml.html;
using iTextSharp.tool.xml.pipeline.css;
using iTextSharp.tool.xml.pipeline.end;
using iTextSharp.tool.xml.parser;
using System.Text;

namespace Eletronuclear.DTDA.COVID_19.UI.Formularios
{
    public partial class frmAuto_Declaracao : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lblnome.Text = Request.QueryString["nome"];
                lblmatricula.Text = Request.QueryString["matricula"];
                lblcpf.Text = Request.QueryString["cpf"];
                lbluo.Text = Request.QueryString["uo"];

                lblGravidezPort.Text = Request.QueryString["gravidezPort"];
                lblGravidezCoab.Text = Request.QueryString["gravidezCoab"];

                lblLactantePort.Text = Request.QueryString["lactantePort"];
                lblLactanteCoab.Text = Request.QueryString["lactanteCoab"];

                lblTranspPort.Text = Request.QueryString["transpPort"];
                lblTranspCoab.Text = Request.QueryString["transpCoab"];

                lblQuimioPort.Text = Request.QueryString["quimioPort"];
                lblQuimioCoab.Text = Request.QueryString["quimioCoab"];

                lblDiabetesPort.Text = Request.QueryString["diabetesPort"];
                lblDiabetesCoab.Text = Request.QueryString["diabetesCoab"];

                lblHiperPort.Text = Request.QueryString["hiperPort"];
                lblHiperCoab.Text = Request.QueryString["hiperCoab"];

                lblTabaPort.Text = Request.QueryString["tabaPort"];
                lblTabaCoab.Text = Request.QueryString["tabaCoab"];

                lblCardioPort.Text = Request.QueryString["cardioPort"];
                lblCardioCoab.Text = Request.QueryString["cardioCoab"];

                lblRenaisPort.Text = Request.QueryString["renaisPort"];
                lblRenaisCoab.Text = Request.QueryString["renaisCoab"];

                lblAutoPort.Text = Request.QueryString["autoPort"];
                lblAutoCoab.Text = Request.QueryString["autoCoab"];

                lblHemaPort.Text = Request.QueryString["hemaPort"];
                lblHemaCoab.Text = Request.QueryString["hemaCoab"];

                lblAsmaPort.Text = Request.QueryString["asmaPort"];
                lblAsmaCoab.Text = Request.QueryString["asmaCoab"];

                lblBronquitePort.Text = Request.QueryString["bronquitePort"];
                lblBronquiteCoab.Text = Request.QueryString["bronquiteCoab"];

                lblEnfisemaPort.Text = Request.QueryString["enfisemaPort"];
                lblEnfisemaCoab.Text = Request.QueryString["enfisemaCoab"];

                lblCriancaPort.Text = Request.QueryString["criancaPort"];

                lblIdosoPort.Text = Request.QueryString["idosoPort"];
                
                lblcondicao.Text = Convert.ToBoolean(Request.QueryString["condicao1"]) ? "esta condição se mantém. " : "não me enquadro mais nas condições acima.";

                lblcidade.Text = Convert.ToBoolean(Request.QueryString["rio"]) ? "Rio de Janeiro" : "Angra dos Reis";

                lblLogin.Text = Request.QueryString["login"];

                lbldata.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");


                this.GerarPDF();                

            }
        }

        #region Metodos

        private void GerarPDF()
        {
            try
            {
                var NomeArquivo = DateTime.Now + "_" + lblLogin.Text + ".pdf";
                NomeArquivo = NomeArquivo.Replace(" ", "_").Replace("/", "-").Replace(":", "_");

                Response.Write("<script language='javascript'> { self.close() }</script>");
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment;filename=" + NomeArquivo);
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                StringWriter sw = new StringWriter(); 
                HtmlTextWriter hw = new HtmlTextWriter(sw);
                this.Page.RenderControl(hw); 

                StringReader sr = new StringReader(sw.ToString());
                Document pdfDoc = new Document(PageSize.A4, 30f, 30f, 20f, 20f);
                HTMLWorker htmlparser = new HTMLWorker(pdfDoc);

                string DiretorioPadrao = Server.MapPath("/ArquivosPdf/" + lblLogin.Text + "/");

                if (!Directory.Exists(DiretorioPadrao))
                {
                    Directory.CreateDirectory(DiretorioPadrao);
                }

                var CaminhoCompleto = Path.Combine(DiretorioPadrao, NomeArquivo);

                this.Salvar(CaminhoCompleto);

                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, new FileStream(CaminhoCompleto, FileMode.Create));
                PdfWriter.GetInstance(pdfDoc, Response.OutputStream);

                pdfDoc.Open();

                string imagepath = Server.MapPath("/Content/imagem/marca.png"); 
                
                iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imagepath);
                image.Alignment = iTextSharp.text.Image.ALIGN_LEFT;
                image.ScaleAbsolute(100f, 42f);

                pdfDoc.Add(image);

                htmlparser.Parse(sr);
                pdfDoc.Close();
                Response.Write(pdfDoc);
                Response.End();                              

            }
            catch (Exception)
            {
                throw;
            }

        }

        private void Salvar(string caminho)
        {
            entidadeTBAUTO_DECLARACAO objAUTO_DECLARACAO;
            regranegocioTBAUTO_DECLARACAO regranegocioTBAUTO_DECLARACAO;

            try
            {
                objAUTO_DECLARACAO = new entidadeTBAUTO_DECLARACAO();
                regranegocioTBAUTO_DECLARACAO = new regranegocioTBAUTO_DECLARACAO();

                objAUTO_DECLARACAO.login = lblLogin.Text;
                objAUTO_DECLARACAO.dthlocal = DateTime.Now;
                objAUTO_DECLARACAO.caminhopdf = caminho;

                regranegocioTBAUTO_DECLARACAO.InserirDados(objAUTO_DECLARACAO);                

            }
            catch (Exception)
            {
                throw;
            }

        }

        #endregion
    }
}