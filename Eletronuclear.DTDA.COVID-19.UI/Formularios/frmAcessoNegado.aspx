﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MpPublica.Master" AutoEventWireup="true" CodeBehind="frmAcessoNegado.aspx.cs" Inherits="Eletronuclear.DTDA.COVID_19.UI.Formularios.frmAcessoNegado" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form runat="server">
        <div>
            <div class="pgConcluidoDiv pgConcluidoTexto centralizarDiv">
                <asp:Label ID="Label1" runat="server" Text="Acesso Negado! Seu usuário não possui acesso."></asp:Label>
            <br />
            </div>

            <div class="pgConcluidoDiv centralizarDiv">
                <asp:Button CssClass="btn btn-primary" ID="btnSair" runat="server" Text="Sair" OnClick="btnSair_Click" />

            </div>
        </div>


    </form>
</asp:Content>
