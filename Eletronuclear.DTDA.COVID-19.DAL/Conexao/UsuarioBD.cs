﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eletronuclear.DTDA.COVID_19.DAL.Conexao
{
    public class UsuarioBD
    {
        public string Servidor { get; set; }
        public string LoginBanco { get; set; }
        public string SenhaBanco { get; set; }
        public string Catalogo { get; set; }
    }

}
