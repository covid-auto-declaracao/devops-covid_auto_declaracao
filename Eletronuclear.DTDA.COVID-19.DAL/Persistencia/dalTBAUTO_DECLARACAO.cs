﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Eletronuclear.DTDA.COVID_19.DAL.Conexao;
using Eletronuclear.DTDA.COVID_19.ENTIDADE;

namespace Eletronuclear.DTDA.COVID_19.DAL.Persistencia
{
    public class dalTBAUTO_DECLARACAO
    {
        public void Adicionar(entidadeTBAUTO_DECLARACAO objTBAUTO_DECLARACAO)
        {
            SqlCommand cmd = null;

            IGerenciadorTransacao objGerenciadorTransacao = null;

            try
            {
                using (objGerenciadorTransacao = FabricaGerenciadorTransacao.GetGerenciador(TipoBDGerenciador.SqlServer))
                {
                    cmd = new SqlCommand(this.Insert);

                    cmd.Parameters.AddWithValue("@login", objTBAUTO_DECLARACAO.login);
                    cmd.Parameters.AddWithValue("@dthlocal", objTBAUTO_DECLARACAO.dthlocal);
                    cmd.Parameters.AddWithValue("@caminhopdf", objTBAUTO_DECLARACAO.caminhopdf);


                    objGerenciadorTransacao.Execute(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        #region " Comandos SQL "

        protected string Insert
        {
            get
            {
                return "INSERT INTO TBAUTO_DECLARACAO(login,dthlocal,caminhopdf) VALUES (@login,@dthlocal,@caminhopdf)";
            }
        }

        #endregion


    }
}
