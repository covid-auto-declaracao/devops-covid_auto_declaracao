﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Eletronuclear.DTDA.COVID_19.DAL.Conexao;
using Eletronuclear.DTDA.COVID_19.ENTIDADE;

namespace Eletronuclear.DTDA.COVID_19.DAL.Persistencia
{
    public class dalUSUARIO
    {
        public DataTable Pesquisar(entidadeUSUARIO objUSUARIO)
        {
            DataTable dt = null;
            SqlCommand cmd = null;
            IGerenciadorTransacao objGerenciadorTransacao = null;

            try
            {
                using (objGerenciadorTransacao = FabricaGerenciadorTransacao.GetGerenciador(TipoBDGerenciador.SqlServer))
                {
                    cmd = new SqlCommand(this.SelectCADASTRO_GERAL);

                    cmd.Parameters.AddWithValue("@LOGIN", objUSUARIO.LOGIN.ToUpper());

                    dt = objGerenciadorTransacao.GetDataTable(cmd);

                    if (dt.Rows.Count == 0)
                    {
                        cmd = null;

                        cmd = new SqlCommand(this.SelectTABELA_AD);

                        cmd.Parameters.AddWithValue("@LOGIN", objUSUARIO.LOGIN.ToUpper());

                        dt = objGerenciadorTransacao.GetDataTable(cmd);
                    }
                }

                return dt;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        #region " Comandos SQL "

        protected string SelectTABELA_AD
        {
            get
            {
                return "SELECT EmployeeID as MATRICULA, DisplayName as NOME_COMPLETO, Department as UO_SIGLA, SamAccountName as LOGIN, '' as CPF, 'TERCEIRIZADO' AS TIPO FROM dbo.TABELA_AD WHERE 1 = 1 AND SamAccountName = @LOGIN ";
            }
        }

        protected string SelectCADASTRO_GERAL
        {
            get
            {
                return "SELECT MATRICULA_P0000 as MATRICULA, NOME_COMPLETO_P0002 as NOME_COMPLETO, UO_SIGLA_P0001 as UO_SIGLA, LOGIN_P0105 as LOGIN, CPF_P0465 as CPF, 'ELETRONUCLEAR' AS TIPO FROM pessoal.TBCADASTRO_GERAL WHERE 1 = 1 AND LOGIN_P0105 = @LOGIN ";
            }
        }
        #endregion
    }
}
